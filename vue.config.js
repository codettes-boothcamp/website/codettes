module.exports = {
  configureWebpack: {
    devServer: {
      port: 9000,
      host: '107.181.244.50',
      liveReload: true
    },
  },
  transpileDependencies: [
    'vuetify'
  ]
}
